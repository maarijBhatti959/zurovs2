let ApiRoute = {
    deleteGym : "api/delete-gym/", //api/delete-gym/12
    newGyms:"api/new-gyms",
    existingGym:'api/existing-gyms',
    feedback:'api/users-feedback',
    seeAllGym:'api/seeall-gyms',
    deleteFeedBack:'api/delete-feedback/',

    // Post Apis
    updatePassword:'api/reset-password',
    sendCode:'api/send-code',
    confirmCode:'api/confirm',


    // GenericApi i-e use for post and get method
    addFaq:'api/faq',


}
export default ApiRoute;