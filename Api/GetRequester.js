let BASE_URL="https://intechsol.co/zurvos";


export async function getRecentsgym(token) {
    return fetch(`${BASE_URL}/api/recents-gym`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        // Authorization: 'Bearer ' + token,
      },
    })
      .then(res => res.json())
      .catch(error => error);
  }

  export async function getRequest(api) {
    return fetch(`${BASE_URL}/`+api, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        // Authorization: 'Bearer ' + token,
      },
    })
      .then(res => res.json())
      .catch(error => error);
  }