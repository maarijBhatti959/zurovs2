let BASE_URL="https://intechsol.co/zurvos";

export async function login(data) {
    return fetch(`${BASE_URL}/api/login`, {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: data,
    })
      .then(res => res.json())
      .catch(error => error);
  }

  export async function PostReq(data,api) {
    return fetch(`${BASE_URL}/`+api, {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: data,
    })
      .then(res => res.json())
      .catch(error => error);
  }
  
  