import React, {useState,useEffect} from 'react';
import {View, TextInput, Text, StyleSheet} from 'react-native';
import Colors from '../../../colors';
import {
  TouchableNativeFeedback,
  ScrollView,
} from 'react-native-gesture-handler';
import {RadioButton} from 'react-native-paper';
import { PostReq } from '../../../Api/Requester';
import ApiRoute from '../../../Api/Apiroute';

const AddNewFaq = ({navigation}) => {
  const [userType, setUserType] = useState('');
  const [loading,setLoading] =useState(false)

  useEffect(() => {
  },[]);

  function addFeqQuestion(){
    setLoading(true)
    let dataJson = {
        type:"video",
        question:"Organize questions by category.",
        answer:"What can also be tedious is listing all the questions in a random order. This is especially frustrating for customers who have several related questions regarding a single topic. Thus, it's helpful if you divide up questions into overarching topics, such as products, security, and billing.",
        video:""
    }
    // "key":"video","type":"file","value":["/C:/Users/My Computer/Downloads/1587116643_.
    PostReq(JSON.stringify(dataJson),ApiRoute.addFaq).then(res => {
      // alert(JSON.stringify(res))
      if(res.status == "success"){
        alert(JSON.stringify(res.message))
      }
      // setRecentAddedArray(res.data)
      setLoading(false)
    })
  }

  return (
    <View style={Styles.container}>
      <ScrollView contentContainerStyle={Styles.innerContainer}>
        <View style={Styles.inputFields}>
          <View style={Styles.inputView2}>
            <Text style={Styles.inputLabelText}>FAQ Type</Text>
            <View style={{height: 15}} />
            <RadioButton.Group
              onValueChange={value => setUserType(value)}
              value={userType}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <View style={Styles.radioView}>
                  <RadioButton value="Questions" />
                  <Text>Questions</Text>
                </View>
                <View style={{width: 15}} />
                <View style={Styles.radioView}>
                  <RadioButton value="Videos" />
                  <Text>Videos</Text>
                </View>
              </View>
            </RadioButton.Group>
          </View>

          <View style={Styles.inputView}>
            <Text style={Styles.inputLabelText}>Questions</Text>
            <TextInput
              placeholder={''}
              keyboardType={'default'}
              style={Styles.inputStyle}
            />
          </View>
          <View style={Styles.inputView}>
            <Text style={Styles.inputLabelText}>Answer</Text>
            <TextInput
              placeholder={''}
              keyboardType={'default'}
              multiline={true}
              style={[
                Styles.inputStyle,
                {height: 100, textAlignVertical: 'top'},
              ]}
            />
          </View>

          <TouchableNativeFeedback
            style={Styles.buttonStyle}
            onPress={() => addFeqQuestion()}>
            <Text style={Styles.loginText}>ADD QUESTIONS</Text>
          </TouchableNativeFeedback>
        </View>
      </ScrollView>
    </View>
  );
};

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_1,
  },
  titleText: {
    fontSize: 30,
    fontWeight: 'bold',
    alignSelf: 'center',
    marginTop: 15,
  },
  innerContainer: {
    paddingHorizontal: 15,
    paddingTop: 20,
    //justifyContent: 'flex-end',
    alignItems: 'center',
    //flex: 1,
  },
  inputFields: {
    width: '100%',
    //bottom: 30,
  },
  inputView: {
    marginBottom: 20,
  },
  inputView2: {
    marginBottom: 20,
  },
  inputLabelText: {
    fontSize: 16,
  },
  inputStyle: {
    backgroundColor: Colors.TEXT_INPUT,
    height: 50,
    marginTop: 5,
    paddingLeft: 15,
    justifyContent: 'center',
    textAlignVertical: 'center',
    fontSize: 16,
    borderWidth: 1.5,
    borderColor: '#dce9f6ff',
    //fontWeight: 'bold',
  },
  checkBoxContainerStyle: {
    backgroundColor: '#f2f7fcff',
    elevation: 0,
    borderWidth: 0,
    paddingLeft: 0,
    marginLeft: 0,
    marginTop: -10,
    marginBottom: 30,
  },
  checkBoxTextStyle: {
    fontSize: 19,
    fontWeight: 'normal',
  },
  buttonStyle: {
    width: '100%',
    backgroundColor: Colors.TOUCHABLE_BUTTON,
    height: 55,
    justifyContent: 'center',
    alignItems: 'center',
    //textAlignVertical: 'center',
  },
  loginText: {
    fontSize: 18,
    fontWeight: 'bold',
    letterSpacing: 1,
    color: '#ffffffff',
  },
  resetPasswordView: {
    marginTop: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  resetPasswordTouchable: {
    paddingHorizontal: 8,
    paddingVertical: 3,
    borderRadius: 50,
  },
  resetPasswordText: {
    fontSize: 19,
  },
  radioView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  deviceuploadBtn: {
    //flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    backgroundColor: '#dee2e7ff',
  },
  deviceuploadBtnText: {
    fontSize: 18,
  },
});

export default AddNewFaq;
