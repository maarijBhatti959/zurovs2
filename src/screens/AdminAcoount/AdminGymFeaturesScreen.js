import React, { useState } from 'react';
import { View, TextInput, Text, StyleSheet } from 'react-native';
import Colors from '../../../colors';
import {
  TouchableNativeFeedback,
  ScrollView,
} from 'react-native-gesture-handler';
import { CheckBox } from 'react-native-elements';
import { getAsyncData } from '../../../assets/Constant/Util';
import { ADDGYMSTEPS } from '../../../assets/Constant/Constants';

const AdminGymFeatures = ({ navigation }) => {
  const [checked, setChecked] = useState(false);
  const [gymDetail, setGymDetail] = useState('');
  const [costPerDay, setCostPerDay] = useState('');

  function addGym() {
    if (gymDetail == "") {
      alert("Enter gym detail")
    } else if (costPerDay == "") {
      alert("Enter cost per day")
    } else {
      getAsyncData(ADDGYMSTEPS.ADDGYMSTEPONE).then(gymUserInfo => {
        getAsyncData(ADDGYMSTEPS.ADDGYMSTEPTWO).then(gymLocation => {
          // let gymUserInfo = {
          //   fullName:fullName,
          //   email:email,
          //   password:password
          // }
          var gymUser = JSON.parse(gymUserInfo);
          //  let gymLocation={
          //   city : city,
          //   zipCode:zipCode,
          //   streetAddress:streetAddress,
          //   phoneNumber:phoneNUmber
          // }
          var gymLoc = JSON.parse(gymLocation);

          let bodyJson = {
            full_name: gymUser.fullName,
            email: gymUser.email,
            password: gymUser.password,
            full_name1: gymLoc.city,
            zipcode: gymLoc.zipCode,
            street_address: gymLoc.streetAddress,
            phoneno: gymLoc.phoneNumber,
            gym_detail:gymDetail,
            cost_per_day:costPerDay
          }
        })
      })
    }

    //  navigation.navigate('AdminAddGym')
  }

  return (
    <View style={Styles.container}>
      <View style={Styles.progressIndicatorBackgroundView}>
        <View style={Styles.progressIndicator} />
      </View>
      <Text style={Styles.titleText}>Features</Text>
      <Text style={Styles.descriptionText}>Enter gym details information</Text>
      <ScrollView contentContainerStyle={Styles.innerContainer}>
        <View style={Styles.inputFields}>
          <View style={Styles.inputView}>
            <Text style={Styles.inputLabelText}>Gym details</Text>
            <TextInput
              placeholder={''}
              style={[Styles.inputStyle, Styles.detailInput]}
              textContentType={'none'}
              multiline={true}
            />
          </View>
          <TouchableNativeFeedback
            onPress={() => null}
            style={[Styles.buttonStyle, Styles.buttonStyle2]}>
            <Text style={Styles.buttonText2}>Add Features</Text>
          </TouchableNativeFeedback>
          <View style={Styles.tagsView}>
            <View style={Styles.tagStyle}>
              <Text style={Styles.tagText}>Free Weight</Text>
            </View>

            <View style={Styles.tagStyle}>
              <Text style={Styles.tagText}>Cardio Machines</Text>
            </View>

            <View style={Styles.tagStyle}>
              <Text style={Styles.tagText}>Setretching Areas</Text>
            </View>
          </View>
          <View style={Styles.inputView}>
            <Text style={Styles.inputLabelText}>Cost per day</Text>
            <TextInput placeholder={'$40'} style={Styles.inputStyle} />
          </View>
          <CheckBox
            title={'Send users details to email'}
            checked={checked}
            uncheckedColor={Colors.COLOR_BLACK}
            checkedColor={Colors.COLOR_BLACK}
            containerStyle={Styles.checkBoxContainerStyle}
            textStyle={Styles.checkBoxTextStyle}
            onPress={() => setChecked(!checked)}
            size={20}
          />

          <View style={Styles.spacer} />

          <TouchableNativeFeedback
            onPress={() => addGym()}
            style={Styles.buttonStyle}>
            <Text style={Styles.buttonText}>Add Gym</Text>
          </TouchableNativeFeedback>
        </View>
      </ScrollView>
    </View>
  );
};

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_1,
  },
  titleText: {
    fontSize: 26,
    fontWeight: 'bold',
    alignSelf: 'center',
    marginTop: 30,
  },
  descriptionText: {
    textAlign: 'center',
    fontSize: 19,
    marginHorizontal: 30,
    marginTop: 0,
    lineHeight: 30,
    color: '#64676bff',
  },
  innerContainer: {
    paddingHorizontal: 15,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 30,
    //flex: 1,
  },
  inputFields: {
    width: '100%',
  },
  inputView: {
    marginBottom: 20,
  },
  inputLabelText: {
    fontSize: 20,
  },
  inputStyle: {
    backgroundColor: Colors.TEXT_INPUT,
    height: 60,
    marginTop: 5,
    paddingLeft: 15,
    justifyContent: 'center',
    textAlignVertical: 'center',
    fontSize: 19,
    elevation: 5,
  },
  detailInput: {
    height: 100,
    textAlignVertical: 'top',
  },
  pickerStyle: {
    width: '100%',
    backgroundColor: Colors.TEXT_INPUT,
    height: 60,
    paddingLeft: 15,
    marginTop: 5,
  },
  pickerItem: {
    fontSize: 22,
  },
  spacer: {
    paddingVertical: 20,
  },
  buttonStyle: {
    width: '100%',
    backgroundColor: Colors.TOUCHABLE_BUTTON,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -20,
  },
  buttonStyle2: {
    backgroundColor: '#dee2e7ff',
    marginTop: 0,
  },
  buttonText: {
    fontSize: 20,
    fontWeight: 'bold',
    letterSpacing: 1,
    color: '#ffffffff',
  },
  buttonText2: {
    fontSize: 20,
    fontWeight: 'bold',
    letterSpacing: 1,
    color: '#64676bff',
  },
  progressIndicatorBackgroundView: {
    backgroundColor: '#f1f1f1ff',
    width: '100%',
  },
  progressIndicator: {
    backgroundColor: '#226ddcff',
    height: 9,
    width: '100%',
  },
  tagsView: {
    marginTop: 25,
    marginBottom: 25,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  tagStyle: {
    paddingHorizontal: 20,
    paddingVertical: 8,
    backgroundColor: '#dbdbdbff',
    borderRadius: 50,
    marginRight: 10,
    marginTop: 10,
  },
  tagText: {
    fontSize: 17,
  },
  checkBoxContainerStyle: {
    backgroundColor: Colors.BACKGROUND_1,
    elevation: 0,
    borderWidth: 0,
    paddingLeft: 0,
    marginLeft: 0,
    marginTop: -10,
  },
  checkBoxTextStyle: {
    fontSize: 16,
    fontWeight: 'normal',
  },
});

export default AdminGymFeatures;
