import React, { useState, useRef, useContext, useReducer, useEffect } from 'react';
import { View, TextInput, Text, StyleSheet, StatusBar } from 'react-native';
import Colors from '../../colors';
import { CheckBox } from 'react-native-elements';
import {
  TouchableNativeFeedback,
  TouchableHighlight,
} from 'react-native-gesture-handler';
import { login, PostReq } from '../../Api/Requester';
import Loader from '../components/Common/Loader';
import { setAsyncData, getAsyncData, ValidateEmail } from '../../assets/Constant/Util';
import { LOGINKEY } from '../../assets/Constant/Constants';
import { ThemeContext } from '../../AppContext';
import ApiRoute from '../../Api/Apiroute';

const Signin = ({ navigation }) => {
  const [checked, setChecked] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  var check = useContext(ThemeContext)
 
  const [isLoader, setLoader] = useState(false);

  // this method work like componentWillMoint...
  const willMount = useRef(true);
  if (willMount.current) {
    console.log('First time load (it runs only once)');
     getAsyncData(LOGINKEY).then(res => {
 
      if (res != null) {
        // this will finish the signinscreen.js screen.
        navigation.replace('AdminAccount');
      } else {
        //  alert(res)
      }

    })

    // setCount(2);
    willMount.current = false;
  } else {
    console.log('Repeated load');
  }
  async function loginProcess() {
    setLoader(true)
    var bodyJson = {
      email: email,
      password: password
    }

    await login(JSON.stringify(bodyJson)).then(res => {
      let data = JSON.stringify(res.data);
      if (checked == true) {
        setAsyncData(LOGINKEY, data)
      }
      navigation.replace(
        email === 'admin@gmail.com'
          ? 'AdminAccount'
          : email === 'gym' ? 'GymManagerAccount' : 'PartenerAccount',
      )
      setLoader(false)
    })
  }

  function resetPasswrod(){

    if(email==''){
      alert('Enter you email!')
    }else{
      if(ValidateEmail(email)){
        setLoader(true)
        let bodyJson={
          email:email
        }
        PostReq(JSON.stringify(bodyJson),ApiRoute.sendCode).then(res=>{
          setLoader(false)
          if(res.status=="error"){
            alert(res.message)
          }
          else{
        // navigation.navigate('ResetPasswordMain')

          }
        })

      }else{
        alert("You have entered an invalid email address!")
      }
    }
  }

  return (
    <View style={Styles.container}>
      <Loader isVisible={isLoader} />
      <StatusBar backgroundColor={'#226ddcff'} />
      <Text style={Styles.titleText}>Sign in</Text>
      <View style={Styles.innerContainer}>
        <View style={Styles.inputFields}>
          <View style={Styles.inputView}>
            <Text style={Styles.inputLabelText}>Email Address{check.provider.state.check}</Text>
            <TextInput
              placeholder={'Your Email Address'}
              textContentType={'emailAddress'}
              keyboardType={'email-address'}
              autoCapitalize={'none'}
              style={Styles.inputStyle}
              onChangeText={text => setEmail(text)}
            />
          </View>
          <View style={Styles.inputView}>
            <Text style={Styles.inputLabelText}>Password</Text>
            <TextInput
              placeholder={'******'}
              secureTextEntry={true}
              onChangeText={text => setPassword(text)}
              style={Styles.inputStyle}
            />
          </View>
          <CheckBox
            title={'Remember login info'}
            checked={checked}
            uncheckedColor={Colors.COLOR_BLACK}
            checkedColor={Colors.COLOR_BLACK}
            containerStyle={Styles.checkBoxContainerStyle}
            textStyle={Styles.checkBoxTextStyle}
            onPress={() => setChecked(!checked)}
          />

          <TouchableNativeFeedback
            style={Styles.buttonStyle}
            onPress={() => loginProcess()}>
            <Text style={Styles.loginText}>LOGIN</Text>
          </TouchableNativeFeedback>

          <View style={Styles.resetPasswordView}>
            <TouchableHighlight
              activeOpacity={0.5}
              underlayColor={'rgba(0,0,0,0.2)'}
              style={Styles.resetPasswordTouchable}
              onPress={() => resetPasswrod()}>
              <Text style={Styles.resetPasswordText}>Reset Password</Text>
            </TouchableHighlight>
          </View>
        </View>
      </View>
    </View>
  );
};

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_1,
  },
  titleText: {
    fontSize: 30,
    fontWeight: 'bold',
    alignSelf: 'center',
    marginTop: 15,
  },
  innerContainer: {
    paddingHorizontal: 15,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  inputFields: {
    width: '100%',
  },
  inputView: {
    marginBottom: 20,
  },
  inputLabelText: {
    fontSize: 20,
  },
  inputStyle: {
    backgroundColor: Colors.TEXT_INPUT,
    height: 60,
    marginTop: 5,
    paddingLeft: 15,
    justifyContent: 'center',
    textAlignVertical: 'center',
    fontSize: 19,
    borderWidth: 1.5,
    borderColor: '#dce9f6ff',
    //fontWeight: 'bold',
  },
  checkBoxContainerStyle: {
    backgroundColor: '#f2f7fcff',
    elevation: 0,
    borderWidth: 0,
    paddingLeft: 0,
    marginLeft: 0,
    marginTop: -10,
    marginBottom: 30,
  },
  checkBoxTextStyle: {
    fontSize: 19,
    fontWeight: 'normal',
  },
  buttonStyle: {
    width: '100%',
    backgroundColor: Colors.TOUCHABLE_BUTTON,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    //textAlignVertical: 'center',
  },
  loginText: {
    fontSize: 20,
    fontWeight: 'bold',
    letterSpacing: 1,
    color: '#ffffffff',
  },
  resetPasswordView: {
    marginTop: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  resetPasswordTouchable: {
    paddingHorizontal: 8,
    paddingVertical: 3,
    borderRadius: 50,
  },
  resetPasswordText: {
    fontSize: 19,
  },
});

export default Signin;
