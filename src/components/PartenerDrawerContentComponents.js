import React from 'react';
import {DrawerContentScrollView} from '@react-navigation/drawer';
import {Text, View, StyleSheet} from 'react-native';
import {Avatar, Divider} from 'react-native-elements';
import Colors from '../../colors';
import {TouchableNativeFeedback} from 'react-native-gesture-handler';

const PartenerDrawerContentComponent = props => {
  return (
    <DrawerContentScrollView
      {...props}
      style={{backgroundColor: Colors.DRAWER_USER_INFO_BACKGROUND}}>
      {/**<DrawerItemList {...props} />
      <DrawerItem
        label="Help"
        onPress={() => props.navigation.navigate('Signin')}
      />**/}
      <View style={Styles.userInfoView}>
        <View style={Styles.avatarView}>
          <Avatar
            source={{
              uri:
                'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
            }}
            showAccessory
            rounded
            size={125}
            accessory={{
              name: 'camera',
              type: 'feather',
              color: '#ffffffff',
            }}
          />
        </View>
        <Text style={Styles.nameText}>Hunter Ballew</Text>
      </View>

      <View style={Styles.drawerItemsListView}>
        <TouchableNativeFeedback
          style={Styles.drawerItem}
          onPress={() => props.navigation.navigate('ContactSupport')}>
          <Text style={Styles.drawerItemText}>Contact Support</Text>
        </TouchableNativeFeedback>

        <Divider style={Styles.divider} />

        <TouchableNativeFeedback
          style={Styles.drawerItem}
          onPress={() => props.navigation.navigate('AdminResetPassword')}>
          <Text style={Styles.drawerItemText}>Reset Password</Text>
        </TouchableNativeFeedback>

        <Divider style={Styles.divider} />

        <TouchableNativeFeedback
          style={Styles.drawerItem}
          onPress={() => props.navigation.navigate('Signin')}>
          <Text style={Styles.drawerItemText}>Logout</Text>
        </TouchableNativeFeedback>
      </View>
    </DrawerContentScrollView>
  );
};

const Styles = StyleSheet.create({
  userInfoView: {
    width: '100%',
    height: 213,
    backgroundColor: Colors.DRAWER_USER_INFO_BACKGROUND,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -3,
  },
  avatarView: {
    width: 125,
    height: 125,
  },
  nameText: {
    fontSize: 22,
    fontWeight: 'bold',
    marginTop: 15,
    color: '#ffffffff',
  },
  drawerItemsListView: {
    width: '98%',
    flex: 1,
    backgroundColor: Colors.DRAWER_BACKGROUND,
    paddingBottom: '73%',
  },
  drawerItem: {
    width: '100%',
    height: 65,
    paddingLeft: 10,
    justifyContent: 'center',
  },
  drawerItemText: {
    fontSize: 20,
    //marginLeft: 10,
  },
  divider: {
    //height: 1,
    marginLeft: 10,
    width: '90%',
  },
});

export default PartenerDrawerContentComponent;
