import React from 'react';
import {View, Text, ActivityIndicator, StyleSheet} from 'react-native';

const Loader = ({isVisible}) => {
  return (
    isVisible && (
      <View style={styles.loadingView} pointerEvents={'none'}>
        <View style={styles.loadingBackgroundView}>
          <ActivityIndicator
            size="large"
            color={styles.loaderColor.color}
            style={styles.commonMargin}
          />
          <Text style={styles.commonMargin}>Loading...</Text>
        </View>
      </View>
    )
  );
};

const styles = StyleSheet.create({
  loadingView: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1,
  },
  loadingBackgroundView: {
    width: 100,
    height: 100,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
    borderRadius: 10,
  },
  loaderColor: {
    color: '#58cd5a',
  },
  commonMargin: {marginTop: 5},
});

export default Loader;
