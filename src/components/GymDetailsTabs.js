import React from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import AdminAllOrdersList from '../screens/AdminAcoount/AdminAllOrdersListScreen';
import nullScreen from '../screens/AdminAcoount/NullScreen';
import Post from '../screens/GymManagerAccout/GymDetails/Post';
import Colors from '../../colors';
import Followers from '../screens/GymManagerAccout/GymDetails/Followers';
import Following from '../screens/GymManagerAccout/GymDetails/Following';

const Tab = createMaterialTopTabNavigator();

const GymDetailsTabs = () => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        labelStyle: {
          fontSize: 18,
          textTransform: 'none',
          color: '#ffffffff',
        },
        indicatorStyle: {
          backgroundColor: '#ffffffff',
        },
        style: {
          backgroundColor: Colors.HEADER_BACKGROUND,
        },
      }}>
      <Tab.Screen
        name="Post"
        component={Post}
        options={{
          title: 'Post',
        }}
      />
      <Tab.Screen
        name="Followers"
        component={Followers}
        options={{
          title: 'Followers',
        }}
      />
      <Tab.Screen
        name="Followings"
        component={Following}
        options={{
          title: 'Following',
        }}
      />
    </Tab.Navigator>
  );
};

export default GymDetailsTabs;
